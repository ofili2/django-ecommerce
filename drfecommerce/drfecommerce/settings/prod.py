from .base import *

SECRET_KEY = secrets.token_urlsafe(64)

ALLOWED_HOSTS = ["*"]
